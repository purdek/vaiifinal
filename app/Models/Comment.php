<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comment extends Model
{
    use HasFactory, Notifiable;
    protected $fillable = [
        'text',
        'clanok_id',
        'user_id',
    ];
    public function clanok(){
        return $this->belongsTo(Clanok::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
