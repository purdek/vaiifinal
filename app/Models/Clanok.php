<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Clanok extends Model
{
    use HasFactory, Notifiable;
    protected $fillable = [
        'title',
        'text',
        'user_email',
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function comment(){
        return $this->hasMany(Comment::class);
    }
    public function category(){
        return $this->hasOne(Category::class);
    }
}


