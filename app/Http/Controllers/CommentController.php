<?php

namespace App\Http\Controllers;
use App\Models\Clanok;
use App\Models\Comment;

use Illuminate\Http\Request;
use function MongoDB\BSON\fromJSON;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $clanok = $user->clanok();

        return view('comment.index',['user'=>$user]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('comment.create',['action'=>route('comment.store'),
            'method'=>'post']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
          $clanok = $request->get('clanok_id');
          $text= $request->get('text');
          $user_id = $request->get('user_id');
          $clanky =Clanok::all();

          foreach ($clanky as $moj){
              if($moj->id == $clanok){
                  $aktualny= $moj;
              }
    }       $vysledok = "Uspesne si pridal komentar";
          auth()->user()->comment()->create(['text'=>$text,'clanok_id'=>$clanok, 'user_id'=>$user_id]);
          return response()->json(['result'=>$aktualny, 'vysledok'=>$vysledok]);

       // $request->validate(['text'=>'required'
       //]);
        //dd($request);
        //auth()->user()->comment()->create($request->all());
       // return redirect()->route('clanok.index');
       // return response()->json($request->all);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return view('comment.show', ['comment'=> $comment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {

        return view('comment.edit',['action'=> route('comment.update', $comment->id), 'method' => 'put', 'model' => $comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,Clanok $clanok)
    {
        $request->validate(['title'=>'required'
        ]);

        $clanok->update($request->all());
        return redirect()->route('clanok.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        $user = "succes";

       //return response()->json(['response'=>$user]);

        return redirect()->route('comment.index');
    }
}
