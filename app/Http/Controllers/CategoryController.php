<?php

namespace App\Http\Controllers;

use App\Models\Clanok;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();


        return view('clanok.index',['user'=>$user]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('clanok.create',['action'=>route('clanok.store'),
            'method'=>'post']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate(['title'=>'required'
        ]);
        auth()->user()->clanok()->create($request->all());
        return redirect()->route('clanok.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Clanok $clanok)
    {
        return view('clanok.show', ['clanok'=> $clanok]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Clanok $clanok)
    {

        return view('clanok.edit',['action'=> route('clanok.update', $clanok->id), 'method' => 'put', 'model' => $clanok]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,Clanok $clanok)
    {
        $request->validate(['title'=>'required'
        ]);

        $clanok->update($request->all());
        return redirect()->route('clanok.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Clanok $clanok)
    {
        $clanok->delete();
        $user = auth()->user()->clanok;

        # return response()->json($user, 200);

        return redirect()->route('clanok.index');
    }
}
