<?php

use App\Http\Controllers\ClanokController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//Route::get('/', [App\Http\Controllers\ClanokController::class, 'index']);
Route::get('/home/{user}', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/show/{clanok}', [ClanokController::class, 'show'])->name('clanok.show');


Route::group(['middleware' => ['auth']], function () {
    Route::resource('user', UserController::class);
    Route::resource('clanok', ClanokController::class);
    Route::resource('comment', CommentController::class);
    Route::get('/user/show/{user}', [UserController::class, 'show'])->name('user.show');



    Route::get('user/{user}/delete', [UserController::class, 'destroy'])->name('user.delete');
    Route::get('clanok/{clanok}/delete', [ClanokController::class, 'destroy'])->name('clanok.delete');
    Route::get('comment/{comment}/delete', [CommentController::class, 'destroy'])->name('comment.delete');
});

