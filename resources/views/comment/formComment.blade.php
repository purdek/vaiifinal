<div class="form-group text-danger">
    @foreach ($errors->all() as $error)
        {{ $error }} <br>
    @endforeach
</div>

<form method="post" action="{{ $action }}">
    @csrf
    @method($method)

    <div class="form-group">
        <label for="text">Text:</label>
        <textarea class="form-control" rows="10" id="text" name="text" >{{old ('text', @$model->text) }}</textarea>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary form-control">
    </div>
</form>
