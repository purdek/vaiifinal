@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading text-center">{{ __('Moje komentare') }}</div>

                    <div class="panel-body" id="telo">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <?php
                            use App\Models\Comment;$comment =Comment::all();
                            ?>
                            @if($user->name == "admin")

                                    @foreach($comment as $coments)

                                        <div class="panel panel-primary " >
                                            <div  class="panel-heading text-center">{{$coments->clanok->title}}

                                            </div>
                                            <div  id="description" class="panel-body text-center">{{$coments->text}}</div>


                                        </div>
                                        <a role="button"  class="btn btn-primary " href="/blog/public/show/{{$coments->clanok->id}}">Zobraz clanok</a>
                                        <a role="button"  class="btn btn-warning " href="/blog/public/comment/{{$coments->id}}/edit">Uprav komentar</a>
                                        <a role="button"  class="btn btn-danger " href="/blog/public/comment/{{$coments->id}}/delete" >Vymaz komentar</a>

                                    @endforeach

                            @elseif($user->comment()->count() != 0 )
                                @foreach($user->comment as $coments)

                                    <div class="panel panel-primary " >
                                        <div  class="panel-heading text-center">{{$coments->clanok->title}}

                                        </div>
                                        <div  id="description" class="panel-body text-center">{{$coments->text}}</div>


                                    </div>
                                    <a role="button"  class="btn btn-primary " href="/blog/public/show/{{$coments->clanok->id}}">Zobraz clanok</a>
                                    <a role="button"  class="btn btn-warning " href="/blog/public/comment/{{$coments->id}}/edit">Uprav komentar</a>
                                    <a role="button"  class="btn btn-danger " href="/blog/public/comment/{{$coments->id}}/delete" >Vymaz komentar</a>

                                @endforeach
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type=text/javascript>
        $(document).ready(function() {
            $("#rem").click(function(e) {
                e.preventDefault();
                var text = $("#text").val();

                var clanok_id = $("#idcko").val();
                var user_id = $("#user").val();
                var _token = $("#_token").val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Content-Type': 'application/json'
                    }
                });
                console.log("no klikol som");
                console.log(text);
                console.log(clanok_id);
                console.log(user_id);
                $.ajax({
                    type: 'DELETE',
                    url: "{{url('/comment/1/delete')}}",
                    data:{clanok_id:clanok_id},

                    success: function (result) {
                        console.log(result);
                    },
                    error: function(){
                        alert("error!!!!");
                    }


                });
                console.log("som za ajaxom");
            });
        });

    </script>


@endsection
