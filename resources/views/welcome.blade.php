<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sem</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="../blog/resources/css/app.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- Styles -->

        <style>
            body {
                font-family: 'Nunito';
            }

        </style>
    </head>
    <body >
    <nav class="navbar navbar-inverse ">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="https://revenuearchitects.com/wp-content/uploads/2017/02/Blog_pic.png">Logo</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ url('/') }}">Domov</a></li>
                    @auth
                        <li><a href="/blog/public/user/show/{{auth()->user()->id}}">Profil</a></li>
                        @can('create', \App\Models\User::class)
                            <li> <a class="nav-link" href="{{ route('user.index') }}">{{ __('Users') }}</a></li>
                        @endcan
                            <li><a class="nav-link" href="{{ route('clanok.index') }}">{{ __('Clanky') }}</a></li>
                            <li><a class="nav-link" href="{{ route('comment.index') }}">{{ __('Moje komentare') }}</a></li>
                    @endauth
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if (Route::has('login'))

                            @auth
                                <li><a class="nav-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                    </form>


                            @else
                                <li> <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a></li>
                                @if (Route::has('register'))
                                    <li> <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a></li>
                                @endif
                            @endauth


                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid text-center ">

        <div class="row mcontent">
            <div class="col-sm-2 bok bg-white">

            </div>
            <div class="col-sm-8 text-center pt-9">
                <h1>Welcome</h1>

                <p><?php
                use App\Models\Clanok;$clanok =Clanok::all();
                ?>
                        <div class="row parentElement">
                        @foreach($clanok as $clanky)
                        <div class="col-sm-4 childElement ">
                        <div class="panel panel-primary  " >
                            <div class="panel-heading text-center ">{{ substr($clanky->title, 0, 30) }}...

                            </div>
                            <div class="panel-body text-center ">{{ substr($clanky->text, 0, 40) }}...</div>

                        </div>
                        <a type="button"  class="btn btn-success " href="/blog/public/show/{{$clanky->id}}">Prejst na clanok</a>
                        </div>
                        @endforeach
                        </div>

                    </p>

            </div>

        </div>
    </div>

    </body>
</html>
