@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="table-responsive ">
        <table class="table">
            <thead>
            <tr>
                <th>Meno</th>
                <th>Email</th>
                <th>Pocet prispevkov</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->clanok()->count()}}</td>
            </tr>

            </tbody>
        </table>
            <a href="/blog/public/user/{{$user->id}}/edit" title="Edit" class="btn btn-sn btn-success">Edit</a>
        </div>
    </div>


@endsection
