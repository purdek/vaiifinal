@extends('layouts.app')

@section('content')
    <div class="container-fluid text-center">

        <div class="row mcontent">
            <div class="col-sm-2 bok bg-white">

            </div>
            <div class="col-sm-8 text-center pt-9">


                <div class="panel panel-primary " >
                    <div class="panel-heading text-center" >{{$clanok->title}}
                        </div>
                    <div class="panel-heading text-center" >{{$clanok->category->title}}
                    </div>
                    <div class="panel-body text-center">{{$clanok->text}}</div>

                </div>
                <div class="panel " id="pan">
                @if($clanok->comment()->count() != 0 )
                @foreach($clanok->comment as $comments)

                    <div class="panel panel-danger " >
                        <div id="title" class="panel-heading text-center">{{$comments->user->email}}</div>
                        <div  id="description" class="panel-body text-center">{{$comments->text}}</div>
                    </div>
                @endforeach
                @endif
                @auth
                </div>
                <form method="post" action="{{route('comment.store')}}" >
                    @csrf
                    {{csrf_field()}}
                    <div class="alert alert-success" role="alert"style="display: none">
                        <a href="#" class="alert-link">result.vysledok</a>
                    </div>
                    <div class="form-group" id="formular">
                        <input type="hidden" name="_token" id="_token" value="{{ Session::token() }}">
                        <label for="text">Text:</label>
                        <textarea class="form-control" rows="4" id="text" name="text" ></textarea>
                        <textarea class="form-control" rows="4" id="idcko" name="idcko" style="display:none" >{{$clanok->id}}</textarea>
                        <textarea class="form-control" rows="4" id="user" name="user"style="display:none" >{{auth()->user()->id}}</textarea>


                    </div>
                    <div class="form-group">
                        <input id="rem" type="submit" class="btn btn-primary form-control">
                    </div>
                </form>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                    <script type=text/javascript>
                        $(document).ready(function() {
                            $("#rem").click(function(e) {
                                e.preventDefault();
                                var text = $("#text").val();
                                var clanok_id = $("#idcko").val();
                                var user_id = $("#user").val();
                                var _token = $("#_token").val();
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                        'Content-Type': 'application/json'
                                    }
                                });
                                $.ajax({
                                    type: 'POST',
                                    url: "{{url('/comment')}}",
                                    dataType:'json',
                                    data: JSON.stringify({
                                        text:text,
                                        clanok_id:clanok_id,
                                        user_id:user_id,
                                        _token :_token
                                    }),
                                    success: function (result) {
                                        console.log(result);
                                        $(".alert").show();
                                        $(".alert").html(result.vysledok);
                                        $("#pan").load(window.location.href + " #pan" );
                                        document.getElementById("text").value = "";

                                    },
                                    error: function(){
                                        alert("error!!!!");
                                    }
                                });
                            });
                        });

                    </script>
                @endauth
            </div>

        </div>
    </div>

@endsection
