@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading text-center">{{ __('Moje clanky') }}</div>

                    <div class="panel-body" id="telo">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <div class="ab-3">
                                <a href="{{ route('clanok.create') }}" class="btn btn-success" role="button">Pridaj clanok</a>
                            </div>

                            <?php
                            use App\Models\Clanok;$clanok =Clanok::all();
                            ?>
                            @if($user->name == "admin")
                                @foreach($clanok as $clanky)

                                    <div class="panel panel-primary " >
                                        <div  class="panel-heading text-center">{{$clanky->title}}

                                        </div>
                                        <div  class="panel-heading text-center">{{$clanky->category->title}}

                                        </div>
                                        <div  id="description" class="panel-body text-center">{{$clanky->text}}</div>


                                    </div>
                                    <a role="button"  class="btn btn-primary " href="/blog/public/show/{{$clanky->id}}">Zobraz clanok</a>
                                    <a role="button"  class="btn btn-warning " href="/blog/public/clanok/{{$clanky->id}}/edit">Uprav clanok</a>
                                    <a role="button"  class="btn btn-danger " href="/blog/public/clanok/{{$clanky->id}}/delete" >Vymaz clanok</a>

                                @endforeach
                            @elseif($user->clanok()->count() != 0 )
                                @foreach($user->clanok as $clanky)

                                    <div class="panel panel-primary " >
                                        <div  class="panel-heading text-center">{{$clanky->title}}
                                            <div  class="panel-heading text-center">{{$clanky->category->title}}

                                            </div>
                                        </div>
                                        <div  id="description" class="panel-body text-center">{{$clanky->text}}</div>


                                    </div>
                                    <a role="button"  class="btn btn-primary " href="/blog/public/show/{{$clanky->id}}">Zobraz clanok</a>
                                    <a role="button"  class="btn btn-warning " href="/blog/public/clanok/{{$clanky->id}}/edit">Uprav clanok</a>
                                    <a role="button"  class="btn btn-danger " href="/blog/public/clanok/{{$clanky->id}}/delete" >Vymaz clanok</a>

                                @endforeach
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
